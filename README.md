## Tidelift / GitLab Example Integration

An example integration between [GitLab Pipelines](https://gitlab.com/) and
the [Tidelift Subscription](https://tidelift.com).

Be sure to change `TL_TEAM` and `TL_PROJECT` in the `.gitlab-ci.yml` and to also
set `TL_TOKEN` in your _CI/CI Settings_ under _Variables_.

If you would like to test this locally, use the [gitlab-runner](https://docs.gitlab.com/runner/):

```
gitlab-runner exec docker tidelift --env TL_TOKEN=mytoken
```

For complete documentation, please see the [Tidelift / GitLab Integration](https://tidelift.com/docs/subscriber/gitlab-integration) page.
